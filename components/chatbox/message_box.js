// Messages is where the messages are stored on the server
Messages = new Meteor.Collection('messages');

var fn = {};

if (Meteor.isClient) {
  // LocalMessages will be what is used for template rendering, stored locally only
  var LocalMessages = new Meteor.Collection(null);

  fn.addLocalMessage = function (e) {
    console.log(e);
  };

  // scrolls message box to bottom
  fn.scrollToBottom = function () {
    var messageBox;

    messageBox = document.querySelector('.message-box');
    messageBox.scrollTop = messageBox.scrollHeight;
  };

  var messagesCursor = Messages.find({
    limit: 5,
    sort: { createdAt: 1 }
  });

  messagesCursor.observe({
    added: function (e) {
      console.log('triggered');
      fn.addLocalMessage(e);
    }
  });

  // Get a cursor for all local messages sorted by createdAt
  var localMessagesCursor = LocalMessages.find({}, {
    sort: { createdAt: 1 }
  });

  // Observe the cursor and apply functions
  localMessagesCursor.observe({
    added: function () {
      fn.scrollToBottom();
    }
  });

  // messages helper returns local messages
  Template.message_box.helpers({
    messages: function () {
      return localMessagesCursor;
    }
  });
}