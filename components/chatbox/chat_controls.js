if (Meteor.isClient) {
  Template.chat_controls.events({
    'submit .new-message-form': function (evt) {
      // Insert new record into messages collection
      Messages.insert({
        content: evt.target.new_message_input.value,
        createdAt: new Date()
      });

      // Clear input
      evt.target.new_message_input.value = "";

      // Prevent page switching
      return false;
    }
  });
}