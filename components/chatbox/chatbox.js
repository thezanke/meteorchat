(function () {
  if (Meteor.isClient) {
    Meteor.startup(function () {
      var messageObserver;

      messageObserver = Messages.find({}, {
        sort: { createdAt: 1 }
      });

      messageObserver.observe({
        added: function () {
          var messageBox;

          messageBox = document.querySelector('.message-box');
          messageBox.scrollTop = messageBox.scrollHeight;
        }
      });
    });
  }
}());